import * as React from 'react';
import {FunctionComponent, useEffect, useState} from 'react';
import firebase from 'firebase';

export enum AuthState {
  LOADING, LOGGED_IN, LOGGED_OUT
}

type UserState = {
  state: AuthState,
  user: firebase.User | null
}

const defaultState: UserState = {
  state: AuthState.LOADING,
  user: null
};

export const AuthContext = React.createContext<UserState>(defaultState);

export const AuthProvider: FunctionComponent = ({children}) => {
  const [user, setUser] = useState<UserState>(defaultState)

  useEffect(() => {
    return firebase.auth().onAuthStateChanged(
      (user) => setUser({
        state: user ? AuthState.LOGGED_IN : AuthState.LOGGED_OUT,
        user,
      })
    );
  }, [])

  return (
    <AuthContext.Provider value={user}>
      {children}
    </AuthContext.Provider>
  )
}