// Import FirebaseAuth and firebase.
import React from 'react';
import firebase from 'firebase';
import {FirebaseAuth} from 'react-firebaseui';
import styled from 'styled-components';

// Configure Firebase.
const config = {
  apiKey: 'AIzaSyBOepNPt5Kk3wka_vA14MxogxpCwvFxLxw',
  authDomain: 'quixy-1337.firebaseapp.com',
};
firebase.initializeApp(config);

// Configure FirebaseUI.
const uiConfig = {
  // Popup signin flow rather than redirect flow.
  // signInFlow: 'redirect',
  // Redirect to /signedIn after sign in is successful. Alternatively you can provide a callbacks.signInSuccess function.
  // signInSuccessUrl: '/',
  // We will display Google and Facebook as auth providers.
  signInOptions: [
    firebase.auth.EmailAuthProvider.PROVIDER_ID,
  ]
};

const StyledLogin = styled.div`
  // override firebase UI styles here...
`

export class Login extends React.Component<{}, {}> {
  render() {
    return (
      <StyledLogin>
        <FirebaseAuth uiConfig={uiConfig} firebaseAuth={firebase.auth()}/>
      </StyledLogin>
    );
  }
}