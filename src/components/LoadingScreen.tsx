import * as React from 'react';
import {Centered} from './Centered';
import {useEffect, useState} from 'react';
import styled from 'styled-components';

const Dots = styled.span`
  display: inline-block;
  width: 50px;
`

export const LoadingScreen = () => {
  const [counter, setCounter] = useState(0);
  useEffect(() => {
    const timer = setInterval(() => {
      setCounter(counter => counter + 1);
    }, 200);

    return () => clearInterval(timer);
  }, []);

  const dots = '...';

  return (
    <Centered>
      <div>
        <span>loading</span>
        <Dots>{dots.slice(0, counter % (dots.length + 1))}</Dots>
      </div>
    </Centered>
  )
}