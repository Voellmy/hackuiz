import styled, {css} from 'styled-components';
import {Link} from 'react-router-dom';

interface ButtonProps {
  lg?: boolean
}

const buttonStyles = css<ButtonProps>`
  font-size: 1em;
  padding: 0.5em 1em;
  color: ${props => props.theme.color};
  border: 2px solid ${props => props.theme.color};
  background-color: transparent;
  font-family: ${props => props.theme.fontFamily};
  cursor: pointer;
  text-decoration: none;
  
  ${props => props.lg && css`
    padding: 1em 2em;
  `}
  
  &:hover {
    color: ${props => props.theme.backgroundColor};
    background-color: ${props => props.theme.color};
  }
`

export const Button = styled.button<ButtonProps>`
  ${buttonStyles}
`
export const LinkButton = styled(Link)<ButtonProps>`
  ${buttonStyles}
`
