import * as React from 'react'
import styled from 'styled-components';

const FullHeight = styled.div`
  height: 100vh;
  display: flex;
  flex-direction: column;
`

const Header = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: ${props => props.theme.color};
  color: ${props => props.theme.backgroundColor};
  padding:  1em;
  
  h1 {
    font-size: 1.5em;
    margin: 0;
  }
`

const Main = styled.main`
  display: flex;
  flex-direction: column;
  flex: 1;
`

const InputWrapper = styled.div`
  position: relative;
  flex: 0;
`

const Input = styled.input`
  width: 100%;
  border: 2px solid ${props => props.theme.color};
  color: ${props => props.theme.color};
  background-color: ${props => props.theme.backgroundColor};
  font-size: 1em;
  padding: 0.5em 1.5em;
  
  &:focus {
    outline: none;
    border-color: ${props => props.theme.highlight};
  }
`

const Output = styled.div`
  flex: 1;
`

const Caret = styled.span`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0.5em;
  display: flex;
  flex-direction: column;
  justify-content: center;
`

interface Props {
  user: firebase.User | null
}

export const MainLayout = ({user}: Props) => {
  return (
    <FullHeight>
      <Header>
        <h1>Hackuiz</h1>
        <span>{user?.displayName}</span>
      </Header>
      <Main>
        <Output>
          <div> [system] > All systems loaded</div>
          <div> [system] > Welcome back, {user?.displayName}</div>
        </Output>
        <InputWrapper>
          <Caret> > </Caret>
          <Input />
        </InputWrapper>
      </Main>
    </FullHeight>
  )
}