import * as React from 'react';
import {useContext} from 'react';
import {Route} from 'react-router-dom';
import {AuthContext, AuthState} from './AuthContext';
import {Login} from './Login';
import {RouteProps} from 'react-router';
import {LoadingScreen} from './LoadingScreen';

export const ProtectedRoute = ({component: Component, ...rest}: RouteProps & {component: React.ComponentType}) => {

  const auth = useContext(AuthContext);

  return (
    <Route {...rest} render={(props) => {
      switch (auth.state) {
        case AuthState.LOADING:
          return <LoadingScreen />
        case AuthState.LOGGED_OUT:
          return <Login/>
        case AuthState.LOGGED_IN:
          return <Component {...props} />
      }
    }}/>
  )
}