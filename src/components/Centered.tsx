import styled from 'styled-components';

export const Centered = styled.div`
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`
