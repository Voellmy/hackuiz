import React from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import {ProtectedRoute} from './components/ProtectedRoute';
import {PlayPage} from './pages/PlayPage';
import {AuthProvider} from './components/AuthContext';
import styled, {ThemeProvider} from 'styled-components'
import './styles/global.scss'
import theme from './styles/theme';
import {StartPage} from './pages/StartPage';

const BaseStyles = styled.div`
  background-color: ${props => props.theme.backgroundColor};
  color: ${props => props.theme.color};
  font-family: ${props => props.theme.fontFamily};
  
  input {
    font-family: ${props => props.theme.fontFamily};
  }
  
  * {
    box-sizing: border-box;
  }
`;

function App() {
  return (
    <ThemeProvider theme={theme}>
      <BaseStyles>
        <AuthProvider>
          <Router>
            <Switch>
              <Route exact path="/" component={StartPage} />
              <ProtectedRoute path="/play" component={PlayPage} />
            </Switch>
          </Router>
        </AuthProvider>
      </BaseStyles>
    </ThemeProvider>
  )
}

export default App;
