import * as React from 'react';
import {LinkButton} from '../components/Button';
import {Centered} from '../components/Centered';

export const StartPage = () => {
  return (
    <Centered>
      <LinkButton lg to="/play">Start</LinkButton>
    </Centered>
  )
}