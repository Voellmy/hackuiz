import * as React from 'react';
import {useContext} from 'react';
import {AuthContext} from '../components/AuthContext';
import {MainLayout} from '../components/MainLayout';

export const PlayPage = () => {
  const {user} = useContext(AuthContext);

  return (
    <>
      <MainLayout user={user}/>
    </>
  );
}