const primaryColor = '#00FF00';
const highlightColor = '#d5ffd5';

export default {
  backgroundColor: '#232323',
  color: primaryColor,
  highlight: highlightColor,
  fontFamily: "'Source Code Pro', monospace",
  link: {
    color: 'lightgreen',
    colorHover: '#FFFFFF',
  }
}